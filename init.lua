-- managing plugins
require('plugins')
-- managing lsp
require('lspconf')
-- keyboard mapping
require('keymaps')
-- local config
require('localconf')
