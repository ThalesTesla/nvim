vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function()
	-- Packer can manage itself
	use 'wbthomason/packer.nvim'
	-- Lsp config
	use {
		'williamboman/nvim-lsp-installer',
		config = function()
			require('conf.nvim-lsp-installer-conf')
		end
	}

    -- rust
	use 'neovim/nvim-lspconfig'

    -- completion plugin
    use 'simrat39/rust-tools.nvim'
    use 'github/copilot.vim'
	use 'hrsh7th/nvim-cmp'
	use 'hrsh7th/cmp-nvim-lsp'
	use 'saadparwaiz1/cmp_luasnip' -- Snippets source for nvim-cmp
	use 'L3MON4D3/LuaSnip' -- Snippets plugin
	use 'hrsh7th/cmp-buffer'
	use 'hrsh7th/cmp-path'
	use 'hrsh7th/cmp-nvim-lua'

    -- A light-weight lsp plugin based on neovim's built-in lsp with a highly
    -- performant UI.
	use {
        'tami5/lspsaga.nvim',
        config = function()
            -- require('conf.lspsaga-conf')
            require('lspsaga').setup{}
        end
    } 

	-- dashboard
	use {
		'goolord/alpha-nvim',
		requires = { 'kyazdani42/nvim-web-devicons' },
		config = function ()
			require'alpha'.setup(require'alpha.themes.startify'.config)
		end
	}

    -- debuger
    use "ravenxrz/DAPInstall.nvim"
    use "ravenxrz/nvim-dap"
    use "theHamsta/nvim-dap-virtual-text"
    use "rcarriga/nvim-dap-ui"
    use "nvim-telescope/telescope-dap.nvim"

    -- Smart and Powerful comment plugin for Neovim. Supports commentstring,
    -- motions, dot-repeat and more.
	use {
		'numToStr/Comment.nvim',
		config = function()
			require('Comment').setup{}
		end
  	}

    -- highlight the word under the cursor
	use 'xiyaowong/nvim-cursorword'

    -- a light-weight and Super Fast statusline plugin
	use({
		"NTBBloodbath/galaxyline.nvim",
		-- your statusline
		config = function()
			require("galaxyline.themes.eviline")
		end,
		-- some optional icons
		requires = { "kyazdani42/nvim-web-devicons", opt = true }
	})

    -- provide some basic functionality such as highlighting based on it
    use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate',
        config = function()
            require('conf.treesitter-conf')
        end
    }

    -- A snazzy buffer line for Neovim built using Lua.
	use {'akinsho/bufferline.nvim',
		tag = "v2.*",
		requires = 'kyazdani42/nvim-web-devicons',
		config = function()
			require('bufferline').setup{}
		end
	}

    -- A simple and fast file explorer tree for Neovim.
	use {
    	'kyazdani42/nvim-tree.lua',
    	requires = {
      		'kyazdani42/nvim-web-devicons', -- optional, for file icon
    	},
    	tag = 'nightly', -- optional, updated every week. (see issue #1193)
		config = function()
			require('nvim-tree').setup{}
		end
	}

    -- a highly extendable fuzzy finder over lists
    use {
        'nvim-telescope/telescope.nvim',
        requires = {
            "nvim-lua/plenary.nvim", -- Lua dev module 
            "BurntSushi/ripgrep", -- text finder
            "sharkdp/fd" -- file finder 
        },
        config = function()
            require('telescope').setup{}
        end
    }

    -- Ultimate smart pairs for Neovim written by Lua.
    use {
        'ZhiyuanLck/smart-pairs',
        event = 'InsertEnter',
        config = function()
            require('pairs'):setup()
        end
    }

    -- A low-contrast dark colorscheme with support for various plugins.
    use {
        "phha/zenburn.nvim",
        config = function()
            require("zenburn").setup()
        end
    }

    -- automatically saving your work whenever you make changes to it
    use{
        "Pocco81/auto-save.nvim",
        config = function()
            require("auto-save").setup {
                -- your config goes here
                -- or just leave it empty :)
            }
        end
    }

    -- Simple plugin for showing files in commit history
    use 'rafcamlet/nvim-whid'

    -- motion
    use 'ggandor/leap.nvim'
    use 'ggandor/flit.nvim'
end)
